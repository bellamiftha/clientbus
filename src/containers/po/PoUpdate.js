import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';

import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import axios from 'axios';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

const getModalStyle = () => {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`
  };
};

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4
  },
  //style inputan
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit
  },

  button: {
    margin: theme.spacing.unit
  },
  rightIcon: {
    marginLeft: theme.spacing.unit
  }
});

class Add extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      email: null,
      nama: null,
      alamat: null,
      no_hp: null
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.detail = this.detail.bind(this);
    this.update = this.update.bind(this);
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleOpen() {
    this.setState({ open: true });
  }

  handleClose() {
    this.setState({ open: false });
  }

  detail(id) {
    axios.get(`http://localhost:2018/po/${id}`).then(res => {
      this.setState({
        id_po: res.data[0].id_po,
        email: res.data[0].email,
        nama: res.data[0].nama,
        alamat: res.data[0].alamat,
        no_hp: res.data[0].no_hp,
        open: true
      });
    });
  }

  update(id) {
    axios
      .put(`http://localhost:2018/po/${id}`, {
        nama: this.state.nama,
        alamat: this.state.alamat,
        no_hp: this.state.no_hp,
        email: this.state.email
      })
      .then(res => {
        this.setState({
          open: false,
          email: null,
          nama: null,
          alamat: null,
          no_hp: null
        });
        this.props.getData();
      });
  }

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Tooltip
          title="Edit"
          onClick={() => {
            this.detail(this.props.idNya);
          }}>
          <IconButton aria-label="Edit">
            <EditIcon />
          </IconButton>
        </Tooltip>

        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}>
          <div style={getModalStyle()} className={classes.paper}>
            <Typography variant="title" id="modal-title">
              Editt Data
            </Typography>
            <br />
            <Typography variant="subheading" id="simple-modal-description">
              <div className={classes.container}>
                <FormControl className={classes.formControl}>
                  <InputLabel htmlFor="">Nama PO</InputLabel>
                  <Input
                    id=""
                    name="nama"
                    value={this.state.nama}
                    fullWidth={true}
                    onChange={this.handleChange}
                  />
                </FormControl>

                <FormControl className={classes.formControl}>
                  <InputLabel htmlFor="">Email</InputLabel>
                  <Input
                    id=""
                    name="email"
                    value={this.state.email}
                    fullWidth={true}
                    onChange={this.handleChange}
                  />
                </FormControl>

                <FormControl className={classes.formControl}>
                  <InputLabel htmlFor="">Alamat</InputLabel>
                  <Input
                    id=""
                    name="alamat"
                    value={this.state.alamat}
                    fullWidth={true}
                    onChange={this.handleChange}
                  />
                </FormControl>

                <FormControl className={classes.formControl}>
                  <InputLabel htmlFor="">NO HP</InputLabel>
                  <Input
                    id=""
                    name="no_hp"
                    value={this.state.no_hp}
                    fullWidth={true}
                    onChange={this.handleChange}
                  />
                </FormControl>
              </div>
            </Typography>
            <Button
              onClick={() => {
                this.update(this.props.idNya);
              }}
              className={classes.button}
              variant="contained"
              color="secondary">
              Save
              <SaveIcon className={classes.rightIcon} />
            </Button>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

Add.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Add);
