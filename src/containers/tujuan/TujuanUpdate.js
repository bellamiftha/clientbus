import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';

import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import axios from 'axios';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

const getModalStyle = () => {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`
  };
};

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4
  },
  //style inputan
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit
  },

  button: {
    margin: theme.spacing.unit
  },
  rightIcon: {
    marginLeft: theme.spacing.unit
  }
});

class Add extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      nama: null,
      lokasi_awal: null,
      lokasi_tujuan: null
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.detail = this.detail.bind(this);
    this.update = this.update.bind(this);
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleOpen() {
    this.setState({ open: true });
  }

  handleClose() {
    this.setState({ open: false });
  }

  detail(id) {
    axios.get(`http://localhost:2018/tujuan/${id}`).then(res => {
      this.setState({
        id_tujuan: res.data[0].id_tujuan,
        nama: res.data[0].nama,
        lokasi_awal: res.data[0].lokasi_awal,
        lokasi_tujuan: res.data[0].lokasi_tujuan,
        open: true
      });
    });
  }

  update(id) {
    axios
      .put(`http://localhost:2018/tujuan/${id}`, {
        nama: this.state.nama,
        lokasi_awal: this.state.lokasi_awal,
        lokasi_tujuan: this.state.lokasi_tujuan
      })
      .then(res => {
        this.setState({
          open: false,
          nama: null,
          lokasi_awal: null,
          lokasi_tujuan: null
        });
        this.props.getData();
      });
  }

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Tooltip
          title="Edit"
          onClick={() => {
            this.detail(this.props.idNya);
          }}>
          <IconButton aria-label="Edit">
            <EditIcon />
          </IconButton>
        </Tooltip>

        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}>
          <div style={getModalStyle()} className={classes.paper}>
            <Typography variant="title" id="modal-title">
              Editt Data
            </Typography>
            <br />
            <Typography variant="subheading" id="simple-modal-description">
              <div className={classes.container}>
                <FormControl className={classes.formControl}>
                  <InputLabel htmlFor="">Nama </InputLabel>
                  <Input
                    id=""
                    name="nama"
                    value={this.state.nama}
                    fullWidth={true}
                    onChange={this.handleChange}
                  />
                </FormControl>

                <FormControl className={classes.formControl}>
                  <InputLabel htmlFor="">Lokasi Awal</InputLabel>
                  <Input
                    id=""
                    name="lokasi_awal"
                    value={this.state.lokasi_awal}
                    fullWidth={true}
                    onChange={this.handleChange}
                  />
                </FormControl>

                <FormControl className={classes.formControl}>
                  <InputLabel htmlFor="">Lokasi Tujuan</InputLabel>
                  <Input
                    id=""
                    name="lokasi_tujuan"
                    value={this.state.lokasi_tujuan}
                    fullWidth={true}
                    onChange={this.handleChange}
                  />
                </FormControl>
              </div>
            </Typography>
            <Button
              onClick={() => {
                this.update(this.props.idNya);
              }}
              className={classes.button}
              variant="contained"
              color="secondary">
              Save
              <SaveIcon className={classes.rightIcon} />
            </Button>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

Add.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Add);
