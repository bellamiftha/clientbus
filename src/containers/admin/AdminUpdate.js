import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import axios from 'axios';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class AlertDialog extends React.Component {
  state = {
    open: false,
    username: null,
    password: null,
    email: null,
    nama: null,
    jenis_kelamin: '',
    foto: null,
    foto_temp: null
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleSelectedFile = event => {
    this.setState({
      foto: event.target.files[0]
    });
  };

  detail = id => {
    axios.get(`http://localhost:2018/admin/${id}`).then(res => {
      this.setState({
        id_admin: res.data[0].id_admin,
        username: res.data[0].username,
        password: res.data[0].password,
        email: res.data[0].email,
        nama: res.data[0].nama,
        jenis_kelamin: res.data[0].jenis_kelamin,
        foto_temp: res.data[0].foto,
        open: true
      });
    });
  };

  update = id => {
    const data = new FormData();
    data.append('foto', this.state.foto, this.state.foto.name);
    data.append('username', this.state.username);
    data.append('password', this.state.password);
    data.append('email', this.state.email);
    data.append('nama', this.state.nama);
    data.append('jenis_kelamin', this.state.jenis_kelamin);

    axios.put(`http://localhost:2018/admin/${id}`, data).then(res => {
      this.setState({
        open: false,
        username: null,
        password: null,
        email: null,
        nama: null,
        jenis_kelamin: '',
        foto: null
      });
      this.props.getData();
    });
  };

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Button onClick={() => this.detail(this.props.idNya)}>Edit</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <DialogContent>
            <form className={classes.root} autoComplete="off">
              <TextField
                id="filled-name"
                label="Username"
                className={classes.textField}
                name="username"
                value={this.state.username}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />
              <TextField
                id="filled-name"
                label="Password"
                className={classes.textField}
                name="password"
                value={this.state.password}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />
              <TextField
                id="filled-name"
                label="Email"
                className={classes.textField}
                name="email"
                value={this.state.email}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />

              <TextField
                id="filled-name"
                label="Nama"
                className={classes.textField}
                name="nama"
                value={this.state.nama}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="jenis_kelamin">Jenis Kelamin</InputLabel>
                <Select
                  value={this.state.jenis_kelamin}
                  onChange={this.handleChange}
                  input={
                    <FilledInput name="jenis_kelamin" id="jenis_kelamin" />
                  }>
                  <MenuItem value="l">Laki Laki</MenuItem>
                  <MenuItem value="p">Perempuan</MenuItem>
                </Select>
              </FormControl>

              <div>
                <img
                  src={`http://localhost:2018/${this.state.foto_temp}`}
                  alt={this.state.foto_temp}
                  width="80px"
                />
              </div>
              <input type="file" onChange={this.handleSelectedFile} />
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Batal
            </Button>
            <Button
              onClick={() => this.update(this.props.idNya)}
              color="primary"
              autoFocus>
              Simpan
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlertDialog);
