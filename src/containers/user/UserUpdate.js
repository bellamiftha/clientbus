import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import axios from 'axios';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class AlertDialog extends React.Component {
  state = {
    labelWidth: 0,
    open: false,
    email: null,
    password: null,
    no_hp: null,
    nama: null,
    foto: null,
    foto_temp: null,
    jenis_kelamin: '',
    hak_akses: '',
    id_po: '',
    data_po: []
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSelectedFile = event => {
    this.setState({
      foto: event.target.files[0]
    });
  };

  detail = id => {
    axios.get(`http://localhost:2018/user/${id}`).then(res => {
      console.log('resnya', res.data);
      this.setState({
        open: true,
        id_user: res.data[0].id_user,
        email: res.data[0].email,
        password: res.data[0].password,
        no_hp: res.data[0].no_hp,
        nama: res.data[0].nama,
        foto_temp: res.data[0].foto,
        jenis_kelamin: res.data[0].jenis_kelamin,
        hak_akses: res.data[0].hak_akses,
        id_po: res.data[0].id_po
      });
    });
  };

  getPo = () => {
    axios.get(`http://localhost:2018/po/`).then(res => {
      this.setState({
        data_po: res.data
      });
    });
  };

  update = id => {
    const data = new FormData();
    data.append('foto', this.state.foto, this.state.foto.name);
    data.append('email', this.state.email);
    data.append('password', this.state.password);
    data.append('no_hp', this.state.no_hp);
    data.append('nama', this.state.nama);
    data.append('jenis_kelamin', this.state.jenis_kelamin);
    data.append('hak_akses', this.state.hak_akses);
    data.append('id_po', this.state.id_po);

    axios.put(`http://localhost:2018/user/${id}`, data).then(res => {
      this.setState({
        open: false,
        id_user: '',
        email: '',
        password: '',
        no_hp: '',
        nama: '',
        foto: '',
        jenis_kelamin: '',
        hak_akses: '',
        id_po: ''
      });
      this.props.getData();
    });
  };

  // componentDidMount() {
  //   this.getPo();
  // }

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Button onClick={() => this.detail(this.props.idNya)}>Edit</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <DialogContent>
            <form className={classes.root} autoComplete="off">
              <TextField
                id="filled-name"
                label="Email"
                className={classes.textField}
                name="email"
                value={this.state.email}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />
              <TextField
                id="filled-name"
                label="No HP"
                className={classes.textField}
                name="no_hp"
                value={this.state.no_hp}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />
              <TextField
                id="filled-name"
                label="Nama"
                className={classes.textField}
                name="nama"
                value={this.state.nama}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />

              <TextField
                id="filled-name"
                label="Foto"
                className={classes.textField}
                name="foto"
                value={this.state.foto}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="jenis_kelamin">Jenis Kelamin</InputLabel>

                <Select
                  value={this.state.jenis_kelamin}
                  onChange={this.handleChange}
                  input={
                    <FilledInput name="jenis_kelamin" id="jenis_kelamin" />
                  }>
                  <MenuItem value="l">Laki Laki</MenuItem>{' '}
                  <MenuItem value="p">Perempuan</MenuItem>
                </Select>
              </FormControl>

              <div>
                <img
                  src={`http://localhost:2018/${this.state.foto_temp}`}
                  alt={this.state.foto_temp}
                  width="80px"
                />
              </div>
              <input type="file" onChange={this.handleSelectedFile} />

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="hak_akses">Hak Akses</InputLabel>

                <Select
                  value={this.state.hak_akses}
                  onChange={this.handleChange}
                  input={<FilledInput name="hak_akses" id="hak_akses" />}>
                  <MenuItem value="agen">Agen</MenuItem>{' '}
                  <MenuItem value="superadmin">Super Admin</MenuItem>
                  <MenuItem value="admin_po">Admin PO</MenuItem> })}
                </Select>
              </FormControl>

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="id_po">PO</InputLabel>
                <Select
                  value={this.state.id_po}
                  onChange={this.handleChange}
                  input={<FilledInput name="id_po" id="id_po" />}>
                  {this.state.data_po.map(datas => {
                    return (
                      <MenuItem key={datas.id_po} value={datas.id_po}>
                        {datas.nama}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Batal
            </Button>
            <Button
              onClick={() => this.update(this.props.idNya)}
              color="primary"
              autoFocus>
              Simpan
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlertDialog);
