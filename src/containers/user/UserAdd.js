import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import axios from 'axios';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class AlertDialog extends React.Component {
  state = {
    labelWidth: 0,
    open: false,
    email: null,
    password: null,
    no_hp: null,
    nama: null,
    foto: null,
    jenis_kelamin: '',
    hak_akses: '',
    id_po: '',
    data_po: []
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSelectedFile = event => {
    this.setState({
      foto: event.target.files[0]
    });
  };

  getPo = () => {
    axios.get(`http://localhost:2018/po/`).then(res => {
      this.setState({
        data_po: res.data
      });
    });
  };

  create = () => {
    const data = new FormData();
    data.append('foto', this.state.foto, this.state.foto.name);
    data.append('email', this.state.email);
    data.append('password', this.state.password);
    data.append('no_hp', this.state.no_hp);
    data.append('nama', this.state.nama);
    data.append('jenis_kelamin', this.state.jenis_kelamin);
    data.append('hak_akses', this.state.hak_akses);
    data.append('id_po', this.state.id_po);

    axios.post('http://localhost:2018/user', data).then(res => {
      this.setState({
        open: false,
        id_user: '',
        email: '',
        password: '',
        no_hp: '',
        nama: '',
        foto: '',
        jenis_kelamin: '',
        hak_akses: '',
        id_po: ''
      });
      this.props.getData();
    });
  };

  componentWillMount() {
    this.getPo();
  }

  dataForm = [
    {
      title: 'Email',
      name: 'email',
      nilai: this.state.email
    },
    {
      title: 'Password',
      name: 'password',
      nilai: this.state.password
    },
    {
      title: 'No HP',
      name: 'no_hp',
      nilai: this.state.no_hp
    },
    {
      title: 'Nama',
      name: 'nama',
      nilai: this.state.nama
    }
  ];

  render() {
    const { classes } = this.props;

    return (
      <div>
        <Button onClick={this.handleClickOpen}>Add</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <DialogContent>
            <form className={classes.root} autoComplete="off">
              {this.dataForm.map((datas, index) => {
                return (
                  <TextField
                    key={index}
                    id="filled-name"
                    label={datas.title}
                    className={classes.textField}
                    name={datas.name}
                    value={datas.value}
                    fullWidth
                    onChange={this.handleChange}
                    margin="normal"
                    variant="filled"
                  />
                );
              })}

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="jenis_kelamin">Jenis Kelamin</InputLabel>

                <Select
                  value={this.state.jenis_kelamin}
                  onChange={this.handleChange}
                  input={
                    <FilledInput name="jenis_kelamin" id="jenis_kelamin" />
                  }>
                  <MenuItem value="l">Laki Laki</MenuItem>{' '}
                  <MenuItem value="p">Perempuan</MenuItem>
                </Select>
              </FormControl>

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="hak_akses">Hak Akses</InputLabel>

                <Select
                  value={this.state.hak_akses}
                  onChange={this.handleChange}
                  input={<FilledInput name="hak_akses" id="hak_akses" />}>
                  <MenuItem value="agen">Agen</MenuItem>{' '}
                  <MenuItem value="superadmin">Super Admin</MenuItem>
                  <MenuItem value="admin_po">Admin PO</MenuItem> })}
                </Select>
              </FormControl>

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="id_po">PO</InputLabel>
                <Select
                  value={this.state.id_po}
                  onChange={this.handleChange}
                  input={<FilledInput name="id_po" id="id_po" />}>
                  {this.state.data_po.map(datas => {
                    return (
                      <MenuItem key={datas.id_po} value={datas.id_po}>
                        {datas.nama}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
              <input type="file" onChange={this.handleSelectedFile} />
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Batal
            </Button>
            <Button onClick={this.create} color="primary" autoFocus>
              Simpan
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlertDialog);
