import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import axios from 'axios';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class AlertDialog extends React.Component {
  state = {
    labelWidth: 0,
    open: false,
    no_plat: '',
    status: '',
    jumlah_kursi: '',
    tipe_kursi: '',
    id_kelas: '',
    id_po: '',
    data_kelas: [],
    data_po: []
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  getKelas = () => {
    axios.get(`http://localhost:2018/kelas/`).then(res => {
      this.setState({
        data_kelas: res.data
      });
    });
  };

  getPo = () => {
    axios.get(`http://localhost:2018/po/`).then(res => {
      this.setState({
        data_po: res.data
      });
    });
  };

  create = () => {
    console.log('diklik');
    axios
      .post('http://localhost:2018/bus', {
        status: this.state.status,
        jumlah_kursi: this.state.jumlah_kursi,
        tipe_kursi: this.state.tipe_kursi,
        id_kelas: this.state.id_kelas,
        id_po: this.state.id_po,
        no_plat: this.state.no_plat
      })
      .then(res => {
        this.setState({
          open: false,
          no_plat: '',
          status: '',
          jumlah_kursi: '',
          tipe_kursi: '',
          id_kelas: '',
          id_po: ''
        });
        this.props.getData();
      });
  };

  componentWillMount() {
    this.getKelas();

    this.getPo();
  }

  dataForm = [
    {
      title: 'Plat',
      name: 'no_plat',
      nilai: this.state.no_plat
    },
    {
      title: 'Status Bus',
      name: 'status',
      nilai: this.state.status
    },
    {
      title: 'Jumlah Kursi',
      name: 'jumlah_kursi',
      nilai: this.state.jumlah_kursi
    },
    {
      title: 'Tipe Kursi',
      name: 'tipe_kursi',
      nilai: this.state.tipe_kursi
    }
  ];

  render() {
    const { classes } = this.props;

    return (
      <div>
        <Button onClick={this.handleClickOpen}>Add</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <DialogContent>
            <form className={classes.root} autoComplete="off">
              {this.dataForm.map((datas, index) => {
                return (
                  <TextField
                    key={index}
                    id="filled-name"
                    label={datas.title}
                    className={classes.textField}
                    name={datas.name}
                    value={datas.value}
                    fullWidth
                    onChange={this.handleChange}
                    margin="normal"
                    variant="filled"
                  />
                );
              })}
              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="id_kelas">Kelas</InputLabel>
                <Select
                  value={this.state.id_kelas}
                  onChange={this.handleChange}
                  input={<FilledInput name="id_kelas" id="id_kelas" />}>
                  {this.state.data_kelas.map(datas => {
                    return (
                      <MenuItem key={datas.id_kelas} value={datas.id_kelas}>
                        {datas.nama_kelas}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="id_po">PO</InputLabel>
                <Select
                  value={this.state.id_po}
                  onChange={this.handleChange}
                  input={<FilledInput name="id_po" id="id_po" />}>
                  {this.state.data_po.map(datas => {
                    return (
                      <MenuItem key={datas.id_po} value={datas.id_po}>
                        {datas.nama}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Batal
            </Button>
            <Button onClick={this.create} color="primary" autoFocus>
              Simpan
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlertDialog);
