import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import axios from 'axios';
import moment from 'moment';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class AlertDialog extends React.Component {
  state = {
    labelWidth: 0,
    open: false,
    id_transaksi: '',
    bukti_pembayaran: '',
    status_transaksi: '',
    total: '',
    tgl_pesan: moment(new Date()).format('YYYY-MM-DDTHH:mm:ss'),
    expired: moment(new Date()).format('YYYY-MM-DDTHH:mm:ss'),
    id_pemesan: '',
    data_pemesan: []
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  getPemesan = () => {
    axios.get(`http://localhost:2018/pemesan/`).then(res => {
      this.setState({
        data_pemesan: res.data
      });
    });
  };

  create = () => {
    axios
      .post('http://localhost:2018/transaksi', {
        bukti_pembayaran: this.state.bukti_pembayaran,
        status_transaksi: this.state.status_transaksi,
        total: this.state.total,
        tgl_pesan: this.state.tgl_pesan,
        expired: this.state.expired,
        id_pemesan: this.state.id_pemesan
      })
      .then(res => {
        this.setState({
          open: false,
          id_transaksi: '',
          bukti_pembayaran: '',
          status_transaksi: '',
          total: '',
          tgl_pesan: '',
          expired: '',
          id_pemesan: ''
        });
        this.props.getData();
      });
  };

  componentWillMount() {
    this.getPemesan();
  }
  dataForm = [
    {
      title: 'Bukti Pembayaran',
      name: 'bukti_pembayaran',
      nilai: this.state.bukti_pembayaran
    },
    {
      title: 'Total',
      name: 'total',
      nilai: this.state.total
    }
  ];

  render() {
    const { classes } = this.props;

    return (
      <div>
        <Button onClick={this.handleClickOpen}>Add</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <DialogContent>
            <form className={classes.root} autoComplete="off">
              {this.dataForm.map((datas, index) => {
                return (
                  <TextField
                    key={index}
                    id="filled-name"
                    label={datas.title}
                    className={classes.textField}
                    name={datas.name}
                    value={datas.value}
                    fullWidth
                    onChange={this.handleChange}
                    margin="normal"
                    variant="filled"
                  />
                );
              })}
              <TextField
                onChange={this.handleChange}
                id="datetime-local"
                label="Tanggal Pesan"
                type="datetime-local"
                name="tgl_pesan"
                value={this.state.tgl_pesan}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true
                }}
              />
              <TextField
                onChange={this.handleChange}
                id="datetime-local"
                label="Expired"
                type="datetime-local"
                name="expired"
                value={this.state.expired}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true
                }}
              />
              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="status_transaksi">
                  {' '}
                  Status Transaksi{' '}
                </InputLabel>

                <Select
                  value={this.state.status_transaksi}
                  onChange={this.handleChange}
                  input={
                    <FilledInput
                      name="status_transaksi"
                      id="status_transaksi"
                    />
                  }>
                  <MenuItem value="lunas">Lunas</MenuItem>{' '}
                  <MenuItem value="kurang">kurang</MenuItem>
                  <MenuItem value="gagal">Gagal</MenuItem> })}
                </Select>
              </FormControl>

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="id_pemesan">Pemesan</InputLabel>
                <Select
                  value={this.state.id_pemesan}
                  onChange={this.handleChange}
                  input={<FilledInput name="id_pemesan" id="id_pemesan" />}>
                  {this.state.data_pemesan.map(datas => {
                    return (
                      <MenuItem key={datas.id_pemesan} value={datas.id_pemesan}>
                        {datas.nama}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Batal
            </Button>
            <Button onClick={this.create} color="primary" autoFocus>
              Simpan
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlertDialog);
