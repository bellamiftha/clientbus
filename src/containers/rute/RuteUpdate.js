import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import axios from 'axios';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class AlertDialog extends React.Component {
  state = {
    labelWidth: 0,
    open: false,
    nama: null,
    deskripsi: null,
    lama_keberangkatan: null
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  detail = id => {
    axios.get(`http://localhost:2018/rute/${id}`).then(res => {
      this.setState({
        id_rute: res.data[0].id_rute,
        nama: res.data[0].nama,
        deskripsi: res.data[0].deskripsi,
        lama_keberangkatan: res.data[0].lama_keberangkatan,
        open: true
      });
    });
  };

  update = id => {
    axios
      .put(`http://localhost:2018/rute/${id}`, {
        nama: this.state.nama,
        deskripsi: this.state.deskripsi,
        lama_keberangkatan: this.state.lama_keberangkatan
      })
      .then(res => {
        this.setState({
          open: false,
          nama: '',
          deskripsi: '',
          lama_keberangkatan: ''
        });
        this.props.getData();
      });
  };

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Button onClick={() => this.detail(this.props.idNya)}>Edit</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <DialogContent>
            <form className={classes.root} autoComplete="off">
              <TextField
                id="filled-name"
                label="Nama Rute"
                className={classes.textField}
                name="nama"
                value={this.state.nama}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />
              <TextField
                id="filled-name"
                label="Deskripsi"
                className={classes.textField}
                name="deskripsi"
                value={this.state.deskripsi}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />
              <TextField
                id="filled-name"
                label="Lama Keberangkatan"
                className={classes.textField}
                name="lama_keberangkatan"
                value={this.state.lama_keberangkatan}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Batal
            </Button>
            <Button
              onClick={() => this.update(this.props.idNya)}
              color="primary"
              autoFocus>
              Simpan
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlertDialog);
