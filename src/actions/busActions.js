const BUS_ALL = 'BUS_ALL'
const BUS_DETAIL = 'BUS_DETAIL'

const all = (data) => ({
	type:BUS_ALL,
	data,
})

const detail = (data) => ({
	type:BUS_DETAIL,
	data,
})

export {
	BUS_ALL,
	BUS_DETAIL,
	all,
	detail
}